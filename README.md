# GitLab schneidertim.de

## Start

```bash
docker-compose up -d
```

## Update GitLab & GitLab Runner

1) Pull updated images

    ```bash
    docker-compose pull
    ```

2) Re-create and start containers

    ```bash
    docker-compose down 
    docker-compose rm
    docker-compose up -d
    ```

## Add new SSL (Sub)Domain

1) Setup Certificate

    ```bash
    ./init-letsencrypt.sh <subdomain>.schneidertim.de
    docker-compose exec nginx nginx -s reload
    ```

2) Adapt nginx config (like in data/nginx/gitlab.conf) an make sure to use the correct ssl_certificate & ssl_certificate_key

    ```txt
    server {
        listen 443 ssl;
        listen [::]:443 ssl;
        server_name schneidertim.de;
        ssl_certificate /etc/letsencrypt/live/schneidertim.de-0001/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/schneidertim.de-0001/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
        location / {
                proxy_pass http://gitlab:80;
                proxy_redirect off;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Host $server_name;
                }
    }
    ```
